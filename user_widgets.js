$(function(){
    $(document).on('change','.switch-active', function() {
        $.ajax({
            url: '/user/ajax',
            type: 'POST',
            data: {
                method: 'set_user_widget',
                param: {
                    'key': $(this).data('key'),
                    'layout': $(this).data('layout'),
                    'project_id': $(this).data('project'),
                    'active': $(this).prop('checked') === true ? 1 : 0
                }
            },
            success: function (response) {
                if (response.success !== true) {
                    alert('Ошибка:\n'+response.error);
                }
            }
        });
    });

    $(document).on('change','#select_project', function() {
        $('.widgets_setting').html('<div class="block"><div class="text-center"><h3>'+
            '<i class="fa fa-sun-o fa-spin"></i><small><br>Загрузка. Пожалуйста, подождите!</small>'+
            '</h3></div></div>');
        $.ajax({
            url: '/user/ajax',
            type: 'POST',
            data: {
                method: 'load_user_widgets',
                param: {
                    'project_id': $(this).val(),
                }
            },
            success: function (response) {
                if (response.success !== true) {
                    $('.widgets_setting').html('Ошибка:\n'+response.error);
                }
                else{
                    $('.widgets_setting').html(response.data);
                }
            }
        });
    });
});
