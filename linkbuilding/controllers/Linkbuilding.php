<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Class Linkbuilding
 */
class Linkbuilding extends MX_Controller
{
    
    
    /**
     * Linkbuilding::__construct()
     * 
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('linkbuilding_model');
        $this->load->library('grocery_CRUD');
        $this->grocery_crud->set_hmvc($this);
    }

    /**
     * Linkbuilding::index()
     * 
     * 
     */
    public function index()
    {
        $this->setMenuKey(__CLASS__);
        $this->setPageInfo();
        
        $this->view->render('linkbuilding');
    }
    
    /**
     * Linkbuilding::sitesBase()
     * 
     * 
     */
    public function sites_base()
    {
        $this->setMenuKey(__CLASS__.'_'.__FUNCTION__);
        $this->setPageInfo();

        $this->grocery_crud->set_table('linkbuilding_sites_base');
        $this->grocery_crud->unset_view();
        $this->grocery_crud->set_relation('subject_id', 'subject_directory', 'name');
        $this->grocery_crud->set_relation('region_id', 'directory_regions_yandex', 'name');
        $this->grocery_crud->set_relation('type_links_id', 'linkbuilding_type_links', 'name');
        $this->grocery_crud->set_relation('author', 'user_profile user_id', '{firstname} {lastname}');

        $this->grocery_crud->field_type('url','string');
        $this->grocery_crud->field_type('author', 'hidden');
        $this->grocery_crud->field_type('date', 'hidden');

        $this->grocery_crud->unset_fields('whois','whois_free_date','active');
        $this->grocery_crud->columns('url','type_links_id','author','date','subject_id','region_id','comments',
            'only_wo_anchor_link','max_count','active','whois_free_date');

        $this->grocery_crud->display_as('id', 'ID');
        $this->grocery_crud->display_as('url', 'Донор');
        $this->grocery_crud->display_as('type_links_id', 'Тип ссылки');
        $this->grocery_crud->display_as('author', 'Автор');
        $this->grocery_crud->display_as('date', 'Дата добавления');
        $this->grocery_crud->display_as('subject_id', 'Тематика');
        $this->grocery_crud->display_as('region_id', 'Регион');
        $this->grocery_crud->display_as('comments', 'Комментарии');
        $this->grocery_crud->display_as('only_wo_anchor_link', 'Только безанкорные ссылки');
        $this->grocery_crud->display_as('max_count', 'Макс. количество ссылок для размещения');
        $this->grocery_crud->display_as('active', 'Активность донора');
        $this->grocery_crud->display_as('whois_free_date', 'Окончание регистрации домена');

        $this->grocery_crud->unset_texteditor('comments','full_text');
        //Заполняем скрытые поля
        $this->grocery_crud->callback_before_insert(array($this,'addHiddenField'));

        if($this->grocery_crud->getState() == 'add' || $this->grocery_crud->getState() == 'insert_validation') {

            $this->grocery_crud->set_rules('type_links_id', 'Донор - Тип ссылки','trim|required|callback_check_uniq_donor_type');

        }
        $this->grocery_crud->required_fields('url','type_links_id','only_anchor','max_count');
        $this->renderGroceryCrud();
        
    }

	/**
     * Проверяем уникальность пары донор и тип ссылки
     * @param $str
     * @return bool
     */
    public function check_uniq_donor_type($str)
    {

        $url = $this->input->post('url');

        $num_row = $this->db->where("url",$url)->where('type_links_id',$str)->get('linkbuilding_sites_base')->num_rows();
        if ($num_row >= 1)
        {
            $this->form_validation->set_message('check_uniq_donor_type', 'У заданного донара уже есть выбранный тип ссылки');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }
    
    /**
     * Linkbuilding::typeLinks()
     * 
     * 
     */
    public function type_links()
    {
        $this->setMenuKey(__CLASS__.'_'.__FUNCTION__);
        $this->setPageInfo();

        $this->config->load('grocery_crud');
        $this->grocery_crud->set_character_limiter(3000);

        $this->grocery_crud->unset_view();
        $this->grocery_crud->set_table('linkbuilding_type_links');

        $this->grocery_crud->display_as('id', 'ID');
        $this->grocery_crud->display_as('name', 'Тип ссылки');
        
        $this->grocery_crud->required_fields('name');

        $this->renderGroceryCrud();
        
    }
    
    /**
     * Linkbuilding::prices()
     * 
     * 
     */
    public function prices()
    {
        $this->setMenuKey(__CLASS__.'_'.__FUNCTION__);
        $this->setPageInfo();
        
        $this->grocery_crud->set_table('linkbuilding_price_links');
        $this->grocery_crud->set_relation('donor_id', 'linkbuilding_sites_base', 'url');
        $this->grocery_crud->unset_view();
        $this->grocery_crud->display_as('donor_id', 'Донор из базы площадок');
        $this->grocery_crud->display_as('price', 'Цена');
        $this->grocery_crud->display_as('date', 'Дата на которую актуальна цена');

        //Заполняем скрытые поля
        $this->grocery_crud->callback_before_insert(array($this,'addHiddenField'));

        $this->grocery_crud->required_fields('donor_id','price');

        $this->renderGroceryCrud();
        
    }

    /**
     * Страница Основная таблица ссылок
     * Linkbuilding::links()
     *
     * @param null $project_id
     * @param null $action
     * @param null $link_id
     * @param null $donor_id
     * @param null $type_links_id
     * @param null $operation_id
     */
    public function links($project_id = null, $action = null, $link_id = null,
                          $donor_id = null, $type_links_id = null, $operation_id = null)
    {
        if($this->grocery_crud->getState() == 'edit' ) {
            $cur_link = $this->linkbuilding_model->getLinkByID($this->uri->segment(5));
            $donor_id = $cur_link['donor_id'];
            $type_links_id =$cur_link['type_links_id'];

        }
        $this->config->load('grocery_crud');
        $this->grocery_crud->set_character_limiter(20);

        $this->load->model('projects/project_model');
        $project=$this->project_model->getOne($project_id);
        $this->cur_progect_id = $project_id;
        $this->cur_progect_name = $project['name_alt'];

        $this->setMenuKey(__CLASS__.'_'.__FUNCTION__ . $project_id);
        $this->setPageInfo(['project_name_alt' => arrayGet('name_alt', $project, '')], $project_id);
        if($project_id){
            $this->validateProjectId($project_id);
        }
        
        $this->grocery_crud->set_table('linkbuilding_links');
        $this->grocery_crud->where('project_id', $project_id);

        $this->grocery_crud->unset_view();

        $this->grocery_crud->set_relation('project_id', 'projects', 'name_alt',
        '(projects.id IN (SELECT project_id FROM projects_users WHERE user_id = '.getCurrentUserId().') 
                OR projects.owner = '.getCurrentUserId().' ) 
                AND projects.id IN 
                (SELECT project_id FROM profile_projects WHERE `key` = "use_linkbuilding" AND `value` = 1)
                AND projects.active = 1'
        );

        $this->grocery_crud->set_relation('type_links_id', 'linkbuilding_type_links',
            'name');

        //$this->grocery_crud->set_relation('executer', 'user_profile user_id', '{firstname} {lastname}');
        $this->grocery_crud->set_relation('donor_id', 'linkbuilding_sites_base',
            '{url}');

        $this->grocery_crud->field_type('url','string');
        $this->grocery_crud->field_type('date', 'hidden');
        $this->grocery_crud->field_type('date_check', 'hidden');
        $this->grocery_crud->field_type('flag_anchor', 'hidden');
        $this->grocery_crud->field_type('executer', 'hidden');

        //Если нет прав на изменение статуса ссылок
        if(!hasPermission(ROLE_RULE_LINKBUILDING_CHANGE_STATUS)) {
            $this->grocery_crud->field_type('status','hidden', linkbuilding_model::STATUS_WAIT_ACCEPTED);
        } else {
            $this->grocery_crud->set_relation('status', 'linkbuilding_statuses',
                'name');
        }

        $this->grocery_crud->unset_add_fields('date_check');


        //Заполняем скрытые поля
        $this->grocery_crud->callback_before_insert(array($this,'addHiddenField'));

        $this->grocery_crud->columns('date','price','type_links_id','donor_id','anchor','url','flag_anchor','login_url',
                                        'login','password','status','date_check','comments');

        $this->grocery_crud->display_as('project_id', 'Проект');
        $this->grocery_crud->display_as('donor_id', 'Донор из базы площадок');
        $this->grocery_crud->display_as('type_links_id', 'Тип ссылки');
        $this->grocery_crud->display_as('price', 'Цена');
        $this->grocery_crud->display_as('anchor', 'Анкор');
        $this->grocery_crud->display_as('url', 'URL');
        $this->grocery_crud->display_as('flag_anchor', 'Безанкорная ссылка');
        $this->grocery_crud->display_as('date', 'Дата');
        $this->grocery_crud->display_as('login', 'Логин');
        $this->grocery_crud->display_as('password', 'Пароль');
        $this->grocery_crud->display_as('login_url', 'Страница с формой авторизации');
        $this->grocery_crud->display_as('status', 'Статус');
        $this->grocery_crud->display_as('date_check', 'Последняя проверка');
        $this->grocery_crud->display_as('comments', 'Комментарии');

        $this->grocery_crud->unset_texteditor('comments','full_text');
        $this->grocery_crud->callback_column('price',array($this,'price_link_callback'));
        $this->grocery_crud->callback_column('flag_anchor',array($this,'flag_anchor_callback'));
        $this->grocery_crud->callback_column('field_donor_id',array($this,'donor_limiter_callback'));


        if($project_id){
            $this->grocery_crud->callback_field('project_id',array($this,'_callback_project_id'));
        }
        if($donor_id){
            $this->cur_donor_id = $donor_id;
            $this->grocery_crud->callback_field('donor_id',array($this,'_callback_donor_id'));
        }
        if($type_links_id){
            $this->cur_type_links_id = $type_links_id;
            $this->grocery_crud->callback_field('type_links_id',array($this,'_callback_type_links_id'));
        }
        if($operation_id){
            $this->cur_operation_id = $operation_id;
            $this->grocery_crud->callback_field('operation_id',array($this,'_callback_operation_id'));
        }

        $this->grocery_crud->required_fields('url','donor_id','type_links_id','status','project_id');
        $this->renderGroceryCrud();
        
    }

    /**
     * Поле проект
     * @param $value
     * @param $row
     * @return string
     */
    public function _callback_project_id($value, $row)
    {

        return '<div class="col-md-9">
                        <p class="form-control-static">"'.$this->cur_progect_name.'"</p>
                        <input type="hidden" value="'.$this->cur_progect_id.'" name="project_id" class="form-control">
                    </div>';
    }

	/**
     * Поле донор
     * @param $value
     * @param $row
     * @return string
     */
    public function _callback_donor_id($value, $row)
    {
        $donor=$this->linkbuilding_model->getDonorByID($this->cur_donor_id);
        return '<div class="col-md-9">
                        <p class="form-control-static">"'.$donor['url'].'"</p>
                        <input type="hidden" value="'.$donor['id'].'" name="donor_id" class="form-control">
                    </div>';
    }

	/**
     * Поле тип ссылки
     * @param $value
     * @param $row
     * @return string
     */
    public function _callback_type_links_id($value, $row)
    {
        $type_link = $this->linkbuilding_model->getTypesOfLinkByID($this->cur_type_links_id);
        return '<div class="col-md-9">
                        <p class="form-control-static">"'.$type_link['name'].'"</p>
                        <input type="hidden" value="'.$type_link['id'].'" name="type_links_id" class="form-control">
                    </div>';
    }

    /**
     * Поле операция
     * @param $value
     * @param $row
     * @return string
     */
    public function _callback_operation_id($value, $row)
    {
        $this->load->model('projects/operations_model');
            $operation_check = $this->operations_model->getOperationListById($this->cur_operation_id);
        return '<input type="hidden" value="'.$operation_check['id'].'" name="operation_id" class="form-control">';
    }

    /**
     * Заполняем поле цена
     * @param $value
     * @param $row
     * @return mixed
     */
    public function price_link_callback($value, $row)
    {
        $post_array['price'] = $this->linkbuilding_model->getPriceLink(
               $row->donor_id,
               $row->type_links_id);
        return $post_array['price'];
    }

    /**
     * Заполняем поле flag_anchor
     * @param $value
     * @param $row
     * @return string
     */
    public function flag_anchor_callback($value, $row)
    {
        return boolToString($row->flag_anchor);
    }

    /**
     * образать текст donor url до 100 символов
     * @param $value
     * @param $row
     * @return string
     */
    public function donor_limiter_callback($value, $row)
    {
        return stringLimiter($value,50);
    }

    /**
     * Вывод всех проектов пользователя, разрешенных для Линкбилдинг
     * Linkbuilding::projects()
     * 
     * @return void
     */
    public function projects()
    {
        $this->setMenuKey(__CLASS__.'_'.__FUNCTION__);
        $this->setPageInfo();
        
        $projects = $this->linkbuilding_model->getAllProjectsByUserId();
        $this->view->setVars(compact('projects'));
        $this->view->render('user_projects');
    }
    
    /**
     * Вывод Статусы
     * Linkbuilding::statuses()
     * 
     */
    public function statuses()
    {
        $this->setMenuKey(__CLASS__.'_'.__FUNCTION__);
        $this->setPageInfo();
        
        $this->grocery_crud->set_table('linkbuilding_statuses');
        //$this->grocery_crud->columns('id','name');
        $this->grocery_crud->display_as('id', 'ID');
        $this->grocery_crud->display_as('name', 'Статус');
        $this->grocery_crud->unset_view();
        $this->grocery_crud->required_fields('name');
        $this->renderGroceryCrud();
        
    }
    
    /**
     * Заполняем скрытые поля
     * Pages_monitor::addHiddenField()
     * 
     * @param mixed $post_array
     * @return mixed
     */
    public function addHiddenField($post_array)
    {

        //Если нет прав на изменение статуса ссылок
        if(!hasPermission(ROLE_RULE_LINKBUILDING_CHANGE_STATUS)) {
            $post_array['status'] = linkbuilding_model::STATUS_WAIT_ACCEPTED;
        }

        if(isset($post_array['author'])){
            $post_array['date'] = date(FORMAT_DATETIME);
            $post_array['author'] = getCurrentUserId();
        }
        if(isset($post_array['donor_id']) && isset($post_array['url'])){
            if(trim($post_array['anchor']) !== trim($post_array['url']) ){
                $post_array['flag_anchor'] = 1;
            }
            else $post_array['flag_anchor'] = 0;

            $post_array['date'] = date(FORMAT_DATE);
            $post_array['executer'] = getCurrentUserId();
        }
        if(isset($post_array['price'])){
            $post_array['date'] = date(FORMAT_DATE);
        }
        return $post_array;
    }

    /**
     * Рабочая область специалиста
     * Pages_monitor::work()
     *
     * @param null $project_id
     * @param null $operation_id
     */
    public function work($project_id = null,$operation_id = null)
    {
        $this->load->model('projects/project_model');
        $project=$this->project_model->getOne($project_id);

        $operation_check = false;
        if($operation_id){
            $this->load->model('projects/operations_model');
            $operation_check = $this->operations_model->getOperationListById($operation_id);
        }

        $this->setMenuKey(__CLASS__.'_'.__FUNCTION__ . $project_id);
        $this->setPageInfo(['project_name_alt' => arrayGet('name_alt', $project, '')], $project_id);
        if($project_id){
            $this->validateProjectId($project_id);
        }

        $get_types_links = array();
        $limit = 10;
        if($this->input->get("limit") || $this->input->get("ids")){
            $limit = (int)$this->input->get("limit");
            if( $limit < 10 ){
                $limit=10;
            }
            if( $limit > 100 ){
                $limit=100;
            }
            $get_types_links = $this->input->get("ids");
            $datas = $this->linkbuilding_model->returnCollection($project_id, $get_types_links, $limit, true);

        }
        else{
            $datas = $this->linkbuilding_model->returnCollection($project_id, $get_types_links, $limit, false);
        }
        //Статистика - Размещено ссылок (всего/без анкора)
        $count_urls = $this->linkbuilding_model->getStatLinks($project_id);

        $types_links = $this->linkbuilding_model->getTypesOfLinks();
        $array_types_links = arrayToDropdown($types_links,'id','name');

        $this->view->setVars(compact('count_urls','array_types_links','get_types_links',
            'datas','project_id','limit','operation_check'));
        $this->view->render('linkbuilding_work');
    }

    /**
     * Цена ссылок по типу
     * Linkbuilding::prices_by_type()
     *
     *
     */
    public function prices_by_type()
    {
        $this->setMenuKey(__CLASS__.'_'.__FUNCTION__);
        $this->setPageInfo();

        $this->grocery_crud->set_table('linkbuilding_price_by_type');
        $this->grocery_crud->set_relation('type_links_id', 'linkbuilding_type_links', 'name');
        $this->grocery_crud->unset_view();
        $this->grocery_crud->display_as('type_links_id', 'Тип ссылки');
        $this->grocery_crud->display_as('price', 'Цена');
        $this->grocery_crud->display_as('date', 'Дата на которую актуальна цена');

        //Заполняем скрытые поля
        $this->grocery_crud->callback_before_insert(array($this,'addHiddenField'));

        $this->grocery_crud->required_fields('type_links_id','price');

        $this->renderGroceryCrud();

    }

    /**
     * Отчет за период по специалисту
     *
     * @param null $user_id
     * @param bool $date_from
     * @param bool $date_to
     */
    public function report($user_id = null, $date_from=false, $date_to = false)
    {
        $this->setMenuKey(__CLASS__.'_'.__FUNCTION__);
        $this->setPageInfo();

        if($this->input->get('user_id')){
            $user_id = $this->input->get('user_id');
        }
        if($this->input->get('date_from')){
            $date_from = $this->input->get('date_from');
        }
        if($this->input->get('date_to')){
            $date_to = $this->input->get('date_to');
        }
        if(!$user_id){
            $user_id = getCurrentUserId();
        }
        if(!$date_from){
            $date_from = date('Y-m-d', strtotime("last Monday"));
        }
        if(!$date_to){
            $date_to = date('Y-m-d', strtotime("Sunday"));
        }

        $this->load->model('dx_auth/users');
        $users = $this->users->get_all()->result_array();


        $data = $this->linkbuilding_model->getUserReportByPeriod($user_id,$date_from,$date_to);
        $users_filds = [];
        foreach ($users as $user) {
            if($user['user_id']== $user_id){
                $this->breadcrumbs->push('Отчет за период по специалисту: '.$user['firstname'].' '.$user['lastname'],
                    '/linkbuilding/report/'.$user_id.'/'.$date_from.'/'.'/'.$date_to.'/');
            }
            $users_filds[] =[
                'id'=>$user['user_id'],
                'fullname'=>$user['firstname'].' '.$user['lastname']
            ];
        }

        $users = arrayToDropdown($users_filds,'id','fullname');

        $this->view->setVars(compact('users','user_id','date_from','date_to','data'));
        $this->view->render('linkbuilding_report');
    }

    /**
     * Импорт базы площадок в sql-таблицу
     */
    public function site_base_import()
    {
        $title = 'Импорт базы площадок';

        $this->setMenuKey(__CLASS__.'_'.__FUNCTION__);
        $this->setPageInfo();

        $info = $errors = null;

        if (!isset($_FILES['file_upload'])) {
            $columns_config = ['url', 'type_links_id','author','date','subject_id',
                'region_id','comments','only_wo_anchor_link'];
            $config['upload_path'] = config_item('temp_path_absolute');
            $config['allowed_types'] = 'xlsx|xls';
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('file')) {
                $errors = $this->upload->display_errors();
            } else {
                $data = $this->upload->data();

                $this->load->helper('excel_helper');
                $data = ExcelHelper::transformExcelToArray($data['full_path']);

                if ($data) {
                    $info = $this->linkbuilding_model->importSiteBase($data,$columns_config);
                }
            }
        }
        $this->view->setVars(compact( 'errors','info'));
        $this->view->render('linkbuilding_site_base_import');
    }

    /**
     * Страница настроек проекта
     * @param null $project_id
     */
    public function settings($project_id = null)
    {
        if(!$project_id){
            redirect('/linkbuilding/settings_defaults/');
        }
        $this->load->model('profile_model');
        //$this->validateProjectId($project_id);

        $project = $this->project_model->getOne($project_id);

        $this->setMenuKey(__CLASS__.'_'.__FUNCTION__ . $project_id);
        $this->setPageInfo(['project_name' => $project['name']], $project_id);
        $this->validateProjectId($project_id);

        $this->form_validation->set_rules('linkbuilding_everyweek_links', 'Сумма', 'required|integer');

        $success = $errors = false;

        $all_types = $this->linkbuilding_model->getTypesOfLinks();
        $keys[]='linkbuilding_everyweek_links';

        foreach($all_types as $value){
            $keys[$value['id']]='linkbuilding_everyweek_type_link_'. $value['id'];

        }

        $types_setting = $this->profile_model->getValueForProject($keys, $project_id);
        if(!$types_setting){
            foreach($keys as $key){
                $value = profile_model::get($key, $project_id, FORMAT_STRING);
                if($value){
                    $types_setting[$key] = $value;
                }
            }
        }

        $types_array = arrayToDropdown($all_types,'id','name');
        unset($keys[0]);

        $this->form_validation->set_rules('linkbuilding_everyweek_links',
            'linkbuilding_everyweek_links', 'callback_sum_check');
        if($this->input->post()){
            //перед сохранением удаляем параметры типов ссылок
            foreach($keys as $value){
                $this->profile_model->deleteProfileForProject($project_id, $value);
            }
            $data_post = array_diff($this->input->post(), array(0, ''));
            if ($this->form_validation->run($this)) {

                foreach ($data_post as $key =>$value){
                    $this->profile_model->setValueForProject($project_id, $key, $value);
                }
                $success = "Параметры сохранены!";
            } else {

                $errors = $this->form_validation->error_string();
            }
            $types_setting = $data_post;
        }

        $this->view->setVars(compact( 'weekly','all_types','types_array',
            'types_setting','keys','success','errors','project_id'));
        $this->view->render('linkbuilding_settings');
    }

    /**
     * Валидация суммы по типам и общей за неделю
     * @param $total
     * @return bool
     */
    public function sum_check($total)
    {
        $sum=0;
        foreach($this->input->post() as $key => $value){
            if($key!='linkbuilding_everyweek_links'){
                $sum +=(int)$value;
            }
        }
        if ((int)$total != $sum)
        {
            $this->form_validation->set_message('sum_check',
                'Сумма по типам и общая за неделю, не совпадает! Проверьте ещё раз!');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    /**
     * Страница настроек проекта
     *
     */
    public function settings_defaults()
    {
        $this->load->model('profile_model');

        $this->setMenuKey(__CLASS__.'_'.__FUNCTION__);
        $this->setPageInfo();

        $this->form_validation->set_rules('linkbuilding_everyweek_links', 'Сумма', 'required|integer');

        $success = $errors = false;

        $all_types = $this->linkbuilding_model->getTypesOfLinks();
        $keys[]='linkbuilding_everyweek_links';
        foreach($all_types as $value){
            $keys[$value['id']]='linkbuilding_everyweek_type_link_'. $value['id'];
        }
        $types_setting = $this->profile_model->getGlobalValue($keys);
        $types_array = arrayToDropdown($all_types,'id','name');
        unset($keys[0]);

        $this->form_validation->set_rules('linkbuilding_everyweek_links',
            'linkbuilding_everyweek_links', 'callback_sum_check');
        if($this->input->post()){
            //перед сохранением удаляем параметры типов ссылок
            foreach($keys as $value){
                $this->profile_model->deleteGlobalProfile($value);
            }
            $data_post = array_diff($this->input->post(), array(0, ''));
            if ($this->form_validation->run($this)) {

                foreach ($data_post as $key =>$value){
                    $this->profile_model->setGlobalValue($key, $value);
                }
                $success = "Параметры сохранены!";
            } else {

                $errors = $this->form_validation->error_string();
            }
            $types_setting = $data_post;
        }

        $this->view->setVars(compact( 'weekly','all_types','types_array',
            'types_setting','keys','success','errors','project_id'));
        $this->view->render('linkbuilding_settings');
    }

    /**
     *  Импорт ссылок из csv файла
     */
    public function ahrefs_links_import()
    {
        $this->setMenuKey(__CLASS__.'_'.__FUNCTION__);
        $this->setPageInfo();

        $rows = $errors = $project_id = null;

        if (!isset($_FILES['file_upload'])) {
            $config['upload_path'] = config_item('temp_path_absolute');
            $config['allowed_types'] = 'csv|txt';
            $this->load->library('upload', $config);


            if (!$this->upload->do_upload('file')) {
                $errors = $this->upload->display_errors();
            } else {
                $data_f = $this->upload->data();
                $value = file_get_contents($data_f['full_path']);
                if(!mb_detect_encoding($value,'UTF8')){
                    $result = mb_convert_encoding($value,'utf8','UCS-2LE');
                    $temp_file_path = config_item('temp_path_absolute').'temp_ahrefs_links_import.txt';
                    $fh = fopen($temp_file_path, "wb");
                    fwrite($fh, $result);
                    fclose($fh);
                    unlink($data_f['full_path']);
                }
                else{
                    $temp_file_path = $data_f['full_path'];
                }

                if ($data_f) {
                    $rows = $this->linkbuilding_model->importCsvAhrefsLinks($temp_file_path);
                }
            }

        }
        $this->view->setVars(compact( 'errors','rows'));
        $this->view->render('linkbuilding_ahrefs_links_import');
    }
}