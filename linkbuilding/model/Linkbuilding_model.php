<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

/**
 * Class Linkbuilding_model
 * 
 */
class Linkbuilding_model extends MX_Model {
    
    /**
     * Статус страницы - Ожидает одобрения
     */
    const STATUS_WAIT_ACCEPTED = 1;
    
    /**
     * Статус страницы - Размещена
     */
    const STATUS_POSTED = 2;
    
    /**
     * Статус страницы - Не размещена
     */
    const STATUS_DONT_POSTED = 3;

    /**
     * Статус страницы - Донор не доступен по техническим причинам
     */
    const STATUS_DONOR_IS_NOT_AVAILABLE = 4;
    
    /**
     * Массив статусов
     * @var array
     */
    static $statuses = [
        self::STATUS_WAIT_ACCEPTED      => 'Ожидает одобрения',
        self::STATUS_POSTED   => 'Размещена',
        self::STATUS_DONT_POSTED   => 'Не размещена',
        self::STATUS_DONOR_IS_NOT_AVAILABLE   => 'Донор не доступен',
        ];
        
    /**
     * @type string
     */
    public $_table;
    
    /**
     * @type string
     */
    public $_table_linkbuilding_links;
    
    /**
     * @type string
     */
    public $_table_linkbuilding_sites_base;

    /**
     * @type string
     */
    public $_table_linkbuilding_type_links;

    /**
     * @type string
     */
    public $_table_linkbuilding_price_links;

    /**
     * @type string
     */
    public $_table_linkbuilding_price_by_type;

    /**
     * @type string
     */
    public $_table_subject_directory;

    /**
     * @type string
     */
    public $_table_user_profile;

    /**
     * @type string
     */
    public $_table_directory_regions_yandex;

    /**
     * @type string
     */
    public $_table_ahrefs_links;


    /**
     * Pages_experiments_model constructor.
     */
    function __construct()
    {
        parent::__construct();

        $this->_table = $this->getTableName('linkbuilding');
        $this->_table_linkbuilding_links = $this->getTableName('linkbuilding_links');
        $this->_table_linkbuilding_sites_base = $this->getTableName('linkbuilding_sites_base');
        $this->_table_linkbuilding_type_links = $this->getTableName('linkbuilding_type_links');
        $this->_table_linkbuilding_price_links = $this->getTableName('linkbuilding_price_links');
        $this->_table_linkbuilding_price_by_type = $this->getTableName('linkbuilding_price_by_type');
        $this->_table_subject_directory = $this->getTableName('subject_directory');
        $this->_table_user_profile = $this->getTableName('user_profile');
        $this->_table_directory_regions_yandex = $this->getTableName('directory_regions_yandex');
        $this->_table_ahrefs_links = $this->getTableName('linkbuilding_ahrefs_links');
        
    }
    
    /**
     * Вернет все активные проекты пользователя, в которых он является owner'ом 
     * и которые расшарены для него у которых use_linkbuilding = 1.
     * Linkbuilding_model::getAllProjectsByUserId()
     * 
     * @param $user_id
     * @return
     */
    public function getAllProjectsByUserId($user_id = false)
    {
        if(!$user_id) {
            $user_id = getCurrentUserId();
        }
        $query = $this->db->query('SELECT * FROM projects p 
                WHERE (p.id IN (SELECT project_id FROM projects_users WHERE user_id = ?) OR p.owner = ? ) 
                AND p.id IN (SELECT project_id FROM profile_projects WHERE `key` = "use_linkbuilding" AND `value` = 1)
                AND p.active = 1 ORDER BY p.active DESC, p.name ASC', [
                $user_id,
                $user_id,
        ]);
        return $query->result_array();
    }
    
    /**
     * Вернет URL, анкор и его url по вчерашней дате
     * Linkbuilding_model::getAnchorPage()
     * 
     * @return
     */
    public function getAnchorPage()
    {
        return $this->db
            ->select("l.id, l.anchor, l.url AS anchor_url, l.donor_id, sb.url")
            ->from("{$this->_table_linkbuilding_links} l")
            ->join("{$this->_table_linkbuilding_sites_base} sb", "sb.id=l.donor_id")
            ->where('l.date_check != CURDATE() or l.date_check IS NULL')
            ->where('l.status = '. self::STATUS_WAIT_ACCEPTED )
            ->or_where('l.status = '. self::STATUS_DONOR_IS_NOT_AVAILABLE)
            ->get()
            ->row_array();
    }
    
    /**
     * Парсит страницу и проверяет наличие анкора на ней
     * Linkbuilding_model::checkAnchorOfPage()
     * 
     * @return bool
     */
    public function checkAnchorOfPage()
    {
        $data_anchor = $this->getAnchorPage();
        if(!$data_anchor){
            return true;
        }
        //Парсер 1 уровня парсим страницу
        try {
            $parser = new MainParser();
            $response = $parser->make($data_anchor['url']);
            $content =  $response->body;
            $status_code = $response->status_code;
        } catch (Exception $e) {
            $this->setStatus($data_anchor['id'],self::STATUS_DONOR_IS_NOT_AVAILABLE);
            return false;
        }

        if($status_code == 200 || $status_code == 201){
            $result = checkLinksInHtml($content, $data_anchor['anchor_url'], $data_anchor['anchor']);
            if($result){
                $status = self::STATUS_POSTED;
            }
            else{
                $status = self::STATUS_DONT_POSTED;
            }
        }
        else if($status_code == 404){
            $status = self::STATUS_DONT_POSTED;
        }
        else {
            $status = self::STATUS_DONOR_IS_NOT_AVAILABLE;
        }

        $this->setStatus($data_anchor['id'],$status);
        return false;
       
    }
    
    /**
     * Устанавливем статус и дату проверки
     * Linkbuilding_model::setStatus()
     * 
     * @param integer $id
     * @param bool $result
     * @param $status
     * @return object
     */
    public function setStatus($id, $status = false)
    {
        if($status){
            $data['status'] = $status;
        }
        $data['date_check'] = date(FORMAT_DATE);
        return $this->db->update($this->_table_linkbuilding_links, $data, [
            'id' => $id
        ]);
    }

	/**
     * Считаем все ссылки и ссылки без анкора
     * Linkbuilding_model::getStaticLinks()
     *
     * @param null $project_id
     * @return mixed
     */
    public function getStatLinks($project_id = null)
    {
        $all_urls=0;
        $all_wo_anchor=0;

        $query=$this->db->query("SELECT t.id, t.name, COUNT(l.`url`) as all_urls,SUM(l.anchor IS NULL)+SUM(l.anchor like url)+SUM(l.anchor like '')
                  as urls_wo_anchor FROM $this->_table_linkbuilding_type_links as t 
                  LEFT JOIN $this->_table_linkbuilding_links as l ON l.type_links_id=t.id 
                  WHERE l.project_id = $project_id GROUP BY t.id, t.name");
        $type_links = $query->result_array();
        if($type_links){
            foreach($type_links as $value){
                $data[] = $value;
                $all_urls +=(int)$value['all_urls'];
                $all_wo_anchor += (int)$value['urls_wo_anchor'];
            }
            $results=[
                'data'=>$data,
                'all_urls'=>$all_urls,
                'all_wo_anchor'=>$all_wo_anchor
            ];
            return $results;
        }
        return false;
    }

	/**
     * Вернет все типы ссылок
     * Linkbuilding_model::getTypesOfLinks()
     *
     * @return mixed
     */
    public function getTypesOfLinks()
    {
        return $this->db->select('*')
            ->from($this->_table_linkbuilding_type_links)
            ->get()
            ->result_array();
    }

    /**
     * Вернет ссылку по id
     * Linkbuilding_model::getLinkByID()
     * @param $id
     * @return mixed
     */
    public function getLinkByID($id = null)
    {
        return $this->db->select('*')
            ->from($this->_table_linkbuilding_links)
            ->where('id',$id)
            ->get()
            ->row_array();
    }

    /**
     * Вернет тип ссылки по id
     * Linkbuilding_model::getTypesOfLinkByID()
     * @param $id
     * @return mixed
     */
    public function getTypesOfLinkByID($id = null)
    {
        return $this->db->select('*')
            ->from($this->_table_linkbuilding_type_links)
            ->where('id',$id)
            ->get()
            ->row_array();
    }

    /**
     * Вернет донор по id
     * Linkbuilding_model::getTypesOfLinkByID()
     * @param $id
     * @return mixed
     */
    public function getDonorByID($id = null)
    {
        return $this->db->select('*')
            ->from($this->_table_linkbuilding_sites_base)
            ->where('id',$id)
            ->get()
            ->row_array();
    }

    /**
     * Вернет массив анкорных и безанкорных ссылок проекта
     * с вычисленным рекомендуемым количеством анкорных и без анкорных
     * Linkbuilding_model::returnCollection()
     * @param $project_id
     * @param $ids
     * @param int $limit
     * @param bool $filter
     * @return mixed
     */
    public function returnCollection($project_id, $ids, $limit = 10, $filter = true)
    {
        $result=false;
        $used_donors_wo_anchor = array();
        $used_donors_anchor = array();
        $array_not_in = array();

        if($filter){
            $results = $this->db
                ->select("l.*, sb.url as sb_url, sb.max_count,tl.name,sb.only_wo_anchor_link")
                ->from("{$this->_table_linkbuilding_links} l")
                ->join("{$this->_table_linkbuilding_sites_base} sb", "sb.id=l.donor_id")
                ->join("{$this->_table_linkbuilding_type_links} tl", "tl.id=l.type_links_id")
                ->where('l.project_id',$project_id)
                ->where_in('l.type_links_id',$ids)
                ->get()
                ->result_array();
        }
        else{
            //по умолчанию используем настройки из проекта
            $this->load->model('profile_model');
            $all_types = $this->getTypesOfLinks();
            $keys[]='linkbuilding_everyweek_links';
            foreach($all_types as $value){
                $keys[$value['id']]='linkbuilding_everyweek_type_link_'. $value['id'];
            }
            $types_setting = $this->profile_model->getValueForProject($keys, $project_id,false);
            if(!$types_setting){
                foreach($keys as $key){
                    $value = profile_model::get($key, $project_id, FORMAT_STRING);
                    if($value){
                        $types_setting[$key] = $value;
                    }
                }
            }
            if($types_setting){
                $limit = $types_setting["linkbuilding_everyweek_links"];
                unset($types_setting["linkbuilding_everyweek_links"]);
                foreach($types_setting as $key => $value){
                    $id = explode('_', $key);
                    $id = end($id);
                    $ids[]=$id;
                    $types_link[$id] = $value;
                }
                $this->db->where_in('l.type_links_id',$ids);
            }

            $results = $this->db
                ->select("l.*, sb.url as sb_url, sb.max_count,,tl.name,sb.only_wo_anchor_link")
                ->from("{$this->_table_linkbuilding_links} l")
                ->join("{$this->_table_linkbuilding_sites_base} sb", "sb.id=l.donor_id")
                ->join("{$this->_table_linkbuilding_type_links} tl", "tl.id=l.type_links_id")
                ->where('l.project_id',$project_id)
                ->get()
                ->result_array();
        }

        if($results) {
            $count_anchor = 0;
            $count_wo_anchor = 0;
            $use_donors = array();

            foreach ($results as $value) {

                if (empty($value['anchor']) || trim($value['anchor']) == trim($value['url'])) {
                    $count_wo_anchor++;
                } else {
                    $count_anchor++;
                }
                $donor[$value['donor_id']] = [
                    'url' => $value['sb_url'],
                    'max_count' => $value['max_count'],
                    'type_links_id' => $value['type_links_id'],
                    'name' => $value['name'],
                    'only_wo_anchor_link' => $value['only_wo_anchor_link'],
                    'count' => isset($donor[$value['donor_id']]['count']) ? ++$donor[$value['donor_id']]['count'] : 1
                ];
                //доноры которые уже используем
                $array_not_in[] = $value['donor_id'];

                //доноры которые ещё можно использовать
                if (($donor[$value['donor_id']]['max_count'] - $donor[$value['donor_id']]['count']) > 0) {
                    $use_donors[$value['donor_id']] = [
                        'id' => $value['donor_id'],
                        'url' => $value['sb_url'],
                        'max_count' => $value['max_count'],
                        'type_links_id' => $value['type_links_id'],
                        'name' => $value['name'],
                        'login'=> $value['login'],
                        'password'=> $value['password'],
                        'login_url'=> $value['login_url'],
                        'only_wo_anchor_link' => $value['only_wo_anchor_link'],
                        'count' => $donor[$value['donor_id']]['max_count'] - $donor[$value['donor_id']]['count'],
                        'price'=> $this->getPriceLink($value['donor_id'],$value['type_links_id'])
                    ];

                }
            }

            $array_not_in = array_unique($array_not_in);

            if ($use_donors) {
                //сортируем по убыванию места на донорах
                arrayOrderByTwoField($use_donors, 'count', 'id', $sort_1 = SORT_DESC, $sort_2 = SORT_DESC);
                foreach ($use_donors as $val) {
                    if ($val['only_wo_anchor_link']) {
                        $used_donors_wo_anchor[] = $val;
                    } else $used_donors_anchor[] = $val;
                }
            }

        }
        else {
            //если ссылок для проекта нет то по умолчанию
            $count_anchor = 50;
            $count_wo_anchor = 50;
        }
        //выбираем всех доноров кроме используемых
        if($array_not_in ){
            $this->db->where_not_in('sb.id',$array_not_in);
        }

        if($ids){
            foreach ($ids as $id){
                $result[]=$this->prepareWorkLinks($project_id, $id, $limit,
                    isset($types_link[$id])?$types_link[$id]:false, $used_donors_wo_anchor,
                    $used_donors_anchor,$count_anchor, $count_wo_anchor);
            }
        }
        else {
            $result[]=$this->prepareWorkLinks($project_id, $id=null, $limit, $types_link = false, $used_donors_wo_anchor,
                $used_donors_anchor,$count_anchor, $count_wo_anchor);
        }
        //var_dump($result); exit;
        return $result;
    }

    /**
     * Формируем ссылки для рабочей области
     * @param $project_id
     * @param $id
     * @param $limit
     * @param $types_link
     * @param $used_donors_wo_anchor
     * @param $used_donors_anchor
     * @param $count_anchor
     * @param $count_wo_anchor
     * @return bool|mixed
     */
    public function prepareWorkLinks($project_id, $id, $limit, $types_link, $used_donors_wo_anchor,
                                     $used_donors_anchor, $count_anchor, $count_wo_anchor)
    {
        $free_donors_wo_anchor = array();
        $free_donors_anchor = array();
        if($id){
            $this->db->where('sb.type_links_id',$id);
            $limit= $types_link ? $types_link : $limit;
        }
        $results = $this->db
            ->select("sb.*,tl.name")
            ->from("{$this->_table_linkbuilding_sites_base} sb")
            ->join("{$this->_table_linkbuilding_type_links} tl", "tl.id=sb.type_links_id")
            ->where('sb.active', true)
            ->get()
            ->result_array();

        if ($results) {
            foreach ($results as $value) {
                $value['price'] = $this->getPriceLink($value['id'], $value['type_links_id']);
                if ($value['only_wo_anchor_link']) {
                    $free_donors_wo_anchor[] = $value;
                } else {
                    $free_donors_anchor[] = $value;
                }
            }
            //расчет соотношениея и количества доноров для рабочей области
            $count_work_links = $this->calculateWorkLinks($project_id, $limit, $count_anchor, $count_wo_anchor);
            //получаем рекомендованные ссылки
            //[$id ? $results[0]['name']:'default']
            $result = $this->getWorkLinks($free_donors_wo_anchor, $free_donors_anchor,
                $count_work_links, $used_donors_wo_anchor, $used_donors_anchor);
            return $result;
        }
        return false;
    }

    /**
     * Формируем количество ссылок для рабочей области
     * @param $free_donors_wo_anchor
     * @param $free_donors_anchor
     * @param $count_work_links
     * @param $used_donors_wo_anchor
     * @param $used_donors_anchor
     * @return mixed
     */
    public function getWorkLinks($free_donors_wo_anchor, $free_donors_anchor,
                                 $count_work_links, $used_donors_wo_anchor, $used_donors_anchor)
    {
        $result = false;
        //набираем рекомендуемое количество безанкорных доноров
        if($free_donors_wo_anchor && count($free_donors_wo_anchor)>=$count_work_links['work_wo_anchor']){
            //берем необходимое количество безанконых доноров
            array_splice($free_donors_wo_anchor,$count_work_links['work_wo_anchor']);
            $result['ready_donors_wo_anchor'] = $free_donors_wo_anchor;
        }
        elseif($free_donors_wo_anchor) {
            $result['ready_donors_wo_anchor'] = $free_donors_wo_anchor;
            if ($used_donors_wo_anchor &&
                count($used_donors_wo_anchor) >=
                ($count_work_links['work_wo_anchor'] - count($free_donors_wo_anchor))) {

                array_splice($used_donors_wo_anchor,
                    $count_work_links['work_wo_anchor'] - count($free_donors_wo_anchor));
                $result['ready_donors_wo_anchor'] = array_merge($free_donors_wo_anchor, $used_donors_wo_anchor);
            }elseif($used_donors_wo_anchor){
                $result['ready_donors_wo_anchor'] = array_merge($free_donors_wo_anchor, $used_donors_wo_anchor);
            }

            if(count($result['ready_donors_wo_anchor'])<$count_work_links['work_wo_anchor'] && $free_donors_anchor  ){
                if (count($free_donors_anchor) >=
                    ($count_work_links['work_wo_anchor'] - count($result['ready_donors_wo_anchor']))) {

                    //сохраняем массив анконрых без количества взятых для беанкорных
                    $free_donors_anchor_cut = array_slice($free_donors_anchor,
                        $count_work_links['work_wo_anchor'] - count($result['ready_donors_wo_anchor']));
                    //добавляем анкорных к безанкорным
                    array_splice($free_donors_anchor,
                        $count_work_links['work_wo_anchor'] - count($result['ready_donors_wo_anchor']));
                    $result['ready_donors_wo_anchor'] = array_merge($free_donors_wo_anchor, $free_donors_anchor);
                    $free_donors_anchor = $free_donors_anchor_cut;

                }
                else{
                    $result['ready_donors_wo_anchor'] = array_merge($free_donors_wo_anchor, $free_donors_anchor);
                    $free_donors_anchor = array();
                }
            }
        }
        elseif($free_donors_anchor){
            if (count($free_donors_anchor) >= ($count_work_links['work_wo_anchor'])
            ) {
                //сохраняем массив анконрых без количества взятых для беанкорных
                $free_donors_anchor_cut = array_slice($free_donors_anchor,$count_work_links['work_wo_anchor']);
                //добавляем анкорных к безанкорным
                array_splice($free_donors_anchor,$count_work_links['work_wo_anchor']);
                $result['ready_donors_wo_anchor'] = $free_donors_anchor;
                $free_donors_anchor = $free_donors_anchor_cut;
            }
            else{
                $result['ready_donors_wo_anchor'] = $free_donors_anchor;
            }

        }

        //набираем рекомендуемое количество анкорных доноров
        if($free_donors_anchor && count($free_donors_anchor)>=$count_work_links['work_anchor']){
            array_splice($free_donors_anchor,$count_work_links['work_anchor']);
            $result['ready_donors_anchor'] = $free_donors_anchor;
        }
        elseif($free_donors_anchor) {
            if ($used_donors_anchor &&
                count($used_donors_anchor) >= ($count_work_links['work_anchor'] - count($free_donors_anchor))
            ){
                array_splice($used_donors_anchor,
                    $count_work_links['work_anchor'] - count($free_donors_anchor));
                $result['ready_donors_anchor'] = array_merge($free_donors_anchor, $used_donors_anchor);
            }elseif($used_donors_anchor){
                $result['ready_donors_anchor'] = array_merge($free_donors_anchor, $used_donors_anchor);
            }
        }
        return $result;
    }

	/**
     * Расчет соотношения анкорных и безанкорных ссылок
     * @param $project_id
     * @param $limit
     * @param $count_anchor
     * @param $count_wo_anchor
     * @return array
     */
    public function calculateWorkLinks($project_id, $limit, $count_anchor, $count_wo_anchor)
    {
        $result=array();

        $global_percent_anchor = Profile_model::get('percent_anchor', $project_id, FORMAT_STRING);
        $global_percent_work = Profile_model::get('percent_work', $project_id, FORMAT_STRING);

        if(!$global_percent_anchor){
            $global_percent_anchor = 25;
        }
        if(!$global_percent_work){
            $global_percent_work = 25;
        }
        //текущий процент анкорных ссылок
        $percent_anchor = round(($count_anchor*100)/($count_anchor+$count_wo_anchor));
        //если limit большше наличия ссылок то лимит равен наличному количеству ссылок
        if($limit > ($count_anchor+$count_wo_anchor)){
            $limit = $count_anchor+$count_wo_anchor;
        }
        //вычисляем необходимое соотношение анкорных и безанкорных ссылок для работы
        if($percent_anchor > (int)$global_percent_anchor){
            $result['work_anchor'] = round(((int)$global_percent_work*$limit)/100);
            $result['work_wo_anchor'] = $limit - $result['work_anchor'];
        }
        else{
            $result['work_wo_anchor'] = round(((int)$global_percent_work*$limit)/100);
            $result['work_anchor'] = $limit - $result['work_wo_anchor'];
        }

        return $result;
    }

	/**
     * Вернет цену ссылки
     * @param null $donor_id
     * @param null $type_links_id
     * @return mixed
     */
    public function getPriceLink($donor_id = null, $type_links_id = null)
    {
        $query_d = "SELECT d.price as donor_price FROM {$this->_table_linkbuilding_price_links} AS a
                            left join {$this->_table_linkbuilding_price_links} as d on d.`date` = 
                                  (SELECT MAX(`date`) FROM {$this->_table_linkbuilding_price_links} AS b 
                                    WHERE b.donor_id = {$donor_id} )
                            WHERE a.donor_id = {$donor_id} ";
        $price_d = $this->db
            ->query($query_d)
            ->row_array();
        if($price_d['donor_price']){
            $result=$price_d['donor_price'];
        }
        else{
            //получаем цену для типа ссылки
            $query_t = "SELECT t.price as type_price 
                                FROM {$this->_table_linkbuilding_price_by_type} AS a
                                left join {$this->_table_linkbuilding_price_by_type} as t on t.`date` = 
                                      (SELECT MAX(`date`) FROM {$this->_table_linkbuilding_price_by_type} AS c 
                                        WHERE c.type_links_id = {$type_links_id} )
                                WHERE a.type_links_id = {$type_links_id}";
            $price_t = $this->db
                ->query($query_t)
                ->row_array();
            if ($price_t['type_price']){
                $result=$price_t['type_price'];
            }
            else{
                $result='0';
            }
        }
        return $result;
    }

    /**
     * Формируем данные для отчета по специалисту в заданный период
     *
     * @param $user_id
     * @param $date_from
     * @param $date_to
     * @return array|bool
     */
    public function getUserReportByPeriod($user_id, $date_from, $date_to)
    {
        $result_with_price = false;
        $results = $this->db
            ->select("l.anchor,l.url,l.donor_id,l.type_links_id,l.comments, sb.url as sb_url, tl.name ")
            ->from("{$this->_table_linkbuilding_links} l")
            ->join("{$this->_table_linkbuilding_sites_base} sb", "sb.id=l.donor_id")
            ->join("{$this->_table_linkbuilding_type_links} tl", "tl.id=l.type_links_id")
            ->where('l.executer',$user_id)
            ->where('`l.date` >= \''.$date_from.'\' and `l.date` <= \''.$date_to.'\' ')
            ->get()
            ->result_array();
        if($results){
            foreach($results as $value){
                $value['price'] = $this->getPriceLink($value['donor_id'],$value['type_links_id']);
                $result_with_price[] = $value;
            }
        }

        return $result_with_price;
    }

    /**
     * Проверяем доноры на активность и получаем информацию whois о них
     *
     * @return bool | int
     */
    public function checkDonor()
    {
        $data['active'] = false;
        $data['date_check'] = date(FORMAT_DATE);

        $donor = $this->db
            ->select("id, url, whois_free_date, whois, active,date_check")
            ->from("{$this->_table_linkbuilding_sites_base}")
            ->where('date_check != CURDATE() or date_check IS NULL')
            ->get()
            ->row_array();
        if(!$donor){
            return true;
        }

        //Парсер 1 уровня парсим страницу
        try {
            $parser = new MainParser();
            $response = $parser->make($donor['url']);
            $status_code = $response->status_code;
            if($status_code == 200 || $status_code == 201 || $status_code == 404 ){
                $data['active'] = true;
            }
            else {
                $data['blocking_reason'] = "Ошибка ответа сервера <$status_code>";
                $data['active'] = false;
            }
        } catch (Exception $e) {
            $data['blocking_reason'] = "Ошибка парсера";
            $data['active'] = false;
        }
        try {
            $check_block_list = hasInBlockList($donor['url']);
            if($check_block_list){
                $data['blocking_reason'] = "Домен заблокирован на территории РФ";
                $data['active'] = false;
            }
        } catch (Exception $e) {
            return 5; //перезапустить через пять минут
        }

        if(empty($donor['whois'])){
            //Получаем домен второго уровня
            $domain_second_level = domainFromUrl($donor['url'],2);
            try {
                $whois_info = whois($domain_second_level);
                if($whois_info["registered"]){
                    $data['whois'] = serialize($whois_info);
                    $data['whois_free_date'] = isset($whois_info["expires"]) ? $whois_info["expires"] : NULL;
                }
                else{
                    $data['blocking_reason'] = "Истёк срок регистрации домена";
                    $data['active'] = false;
                }
            } catch (Exception $e) {

            }
        }
        $this->updateDonor($donor['id'],$data);

        return false;
    }

    /**
     * Обновляем донор
     * @param $id integer
     * @param $data array
     * @return object
     */
    public function updateDonor($id, $data)
    {
        return $this->db->update($this->_table_linkbuilding_sites_base, $data, [
            'id' => (int)$id
        ]);
    }

    /**
     * Импорт доноров
     * @param array $data
     * @param array $columns_config
     * @return int
     */
    public function importSiteBase(array $data, array $columns_config)
    {

        $to_insert = $import_data = [];
        foreach ($data as $index => $row) {
            if(count($row) === count(array_filter($row, 'is_null'))){
                break;
            }
            if($index == 0) {
                continue;
            }
            $r = [];

            foreach ($row as $col_index => $value) {
                if (isset($columns_config[$col_index])) {
                    switch ($columns_config[$col_index]){
                        case "type_links_id":
                            if(!empty($value)){
                                $n_type_link = mb_ucfirst(trim($value));
                                $type_id = $this->db
                                    ->select("id")
                                    ->from($this->_table_linkbuilding_type_links)
                                    ->where("`name` like '".$n_type_link."'")
                                    ->get()
                                    ->row_array();
                                if(!$type_id){
                                    $data_type['name']=$n_type_link;
                                    $this->db->insert($this->_table_linkbuilding_type_links, $data_type);
                                    $r[$columns_config[$col_index]] = $this->db->insert_id();
                                }
                                else{
                                    $r[$columns_config[$col_index]] = $type_id['id'];
                                }
                            }
                            else{
                                $result['type_links_id'] = 4;//Вопрос-ответ по умолчанию
                            }
                            break;
                        case "subject_id":
                            if(!empty($value) && $value !== '-' && $value !== '.'){
                                $r[$columns_config[$col_index]] = $this->checkSubject(mb_ucfirst(trim($value)));
                            }
                            else{
                                $r[$columns_config[$col_index]] = 1620;// 1620 Общетематический - по умолчанию
                            }
                            break;
                        case "region_id":
                            if(!empty($value)){
                                $n_reg = mb_ucfirst(trim($value));
                                $reg = $this->db
                                    ->select("id")
                                    ->from($this->_table_directory_regions_yandex)
                                    ->where("`name` like '".$n_reg."'")
                                    ->get()
                                    ->row_array();
                                if(empty($reg['id'])){
                                    $r[$columns_config[$col_index]] = 318; //318 - Универсальное
                                }
                                else{
                                    $r[$columns_config[$col_index]] = $reg['id'];
                                }
                            }
                            else{
                                $r[$columns_config[$col_index]] = 318;//по умолчанию
                            }
                            break;
                        case "only_wo_anchor_link":
                            $r[$columns_config[$col_index]] = (!empty($value) && trim($value) == "да") ? 1 : 0;
                            break;
                        case "comments":
                            $r[$columns_config[$col_index]] =  $value;
                            break;
                        case "author":
                            if(!empty($value)){
                                $name = explode(' ',trim($value));
                                if(count($name)>1){
                                    $name[0] = mb_ucfirst($name[0]);
                                    $name[1] = mb_ucfirst($name[1]);
                                    $user = $this->db
                                        ->select("id")
                                        ->from($this->_table_user_profile)
                                        ->where("`lastname` like '".$name[0]."' AND `firstname` like '".$name[1]."' ")
                                        ->or_where("`firstname` like '".$name[0]."' AND `lastname` like '".$name[1]."' ")
                                        ->or_where("`lastname` like '".$name[1]."' ")
                                        ->or_where("`lastname` like '".$name[0]."' ")
                                        ->get()
                                        ->row_array();
                                }
                                else{
                                    $name[0] = mb_ucfirst($name[0]);
                                    $user = $this->db
                                        ->select("id")
                                        ->from($this->_table_user_profile)
                                        ->where("`lastname` like '".$name[0]."'")
                                        ->get()
                                        ->row_array();
                                }
                                $r[$columns_config[$col_index]] = !$user ? 1 : $user['id'];
                            }
                            else{
                                $r[$columns_config[$col_index]] = 1;
                            }
                            break;
                        case "url":
                            $r[$columns_config[$col_index]] = prep_url(trim($value));
                            break;
                        case "date" :
                            $r[$columns_config[$col_index]] =  !empty($value) ? $value : date(FORMAT_DATE);
                            break;
                    }
                }
            }
            //смотрим в комментариях безанкорные ссылки и ставим флаг
            if(!empty($r['comments']) && strpos($r['comments'], 'езанкор') && $r['only_wo_anchor_link'] != 1){
                $r['only_wo_anchor_link'] = 1;
            }
            $import_data[] = $r;
        }
        //уникальные донор  + тип ссылки
        foreach($import_data as $row){
            $sb = $this->db
                ->select("id")
                ->from("linkbuilding_sites_base")
                ->where("(url like '".$row['url']."') or (url like '".$row['url']."/')")
                ->where("type_links_id", $row['type_links_id'])
                ->get()
                ->row_array();
            if(!$sb){
                $to_insert[] = $row;
            }

        }
        if ($to_insert) {
            foreach (array_chunk($to_insert, 500) as $to_insert_part) {
                $this->db->insert_batch($this->_table_linkbuilding_sites_base, $to_insert_part);
            }
        }
        $result['total'] = count($import_data);
        $result['is_sb'] = count($import_data) - count($to_insert);
        $result['import'] = count($to_insert);

        return $result;
    }



    /**
     *  Проверяем наличие тематики  - выбираем или создаем
     * @param $name_subject
     * @return int|mixed
     */
    public function checkSubject($name_subject)
    {
        //создаем новый с заданным родителем
        $new=[
            'Общие вопросы' => 1620,
            'О работе' => 104,
            'Бизнес проекты' => 52,
            'Фильмы' => 322,
            'Отзывы' => 104,
            'Юмор' => 564,
            'Совместные покупки' => 1332,
            'Экономика и финансы' => 111,
            'Программирование' => 494,
            'Linux' => 494,
            'Свадебный' => 1522,
            'Образование' => 58,
            'Беременность, роды, дети' => 63,
            'Ремонт и строительство' => 67,
            'Красота и здоровье' => 55,
            'Оружейный каталог' => 1,
            'Афиша' => 1008,
            'Покупки' => 1,
            'Анонсы событий и мероприятий' => 108,
            'О компаниях' => 108,
            'Digital' => 494,
            'Подарки, сувениры' => 1420,
            'Электрощитовое оборудование' => 943,
        ];

        //заменяем сущестующей тематикой
        $change_subject=[
            'Лекарства' => 375,
            'Искусство' => 56,
            'Интенет-магазины' => 277,
            'Одежда' => 1326,
            'Подарки' => 1420,
            'Цветочные магазины' => 1586,
            'Интернет-агентства, студии' => 275,
            'Фитнес'=>1051,
            'Народная и нетрадиционная медицина' => 445,
            'Здоровье' => 370,
            'Продукты' => 1329,
            'Технологии' => 494,
            'Мото' => 1081,
            'Товары' => 893,
            'Дом и быт' => 1006,
            'Игры' => 588,
            'Рыбалка' => 1063,
            'Новости' => 1115,
            'Навигаторы' => 1303,
            'Аутисты' => 558,
            'Интернет-маркетинг' => 108,
            'Собачники' => 1020,
            'Дети' => 1624,
            'Медицина' => 370,
            'Роды' => 1624,
            'Книги, кино' => 322,
            'Авто' => 2,
            'Косметика' => 297,
            'Здоровье и медицина' => 370,
            'It' => 494,
            'Юриспруденция' => 227,
            'Мамский форум' => 1624,
            'Парикмахерское дело' => 510,
            'Дом и быт' => 63,
            'Дом, семья, хобби' => 63,
            'Гаджеты' => 54
        ];

        $subj = $this->db
            ->select("id")
            ->from($this->_table_subject_directory)
            ->where("`name` like '".$name_subject."'")
            ->get()
            ->row_array();

        if(!$subj){
            if(key_exists($name_subject, $new)){
                $data_subject['parent'] = $new[$name_subject];
                $data_subject['name'] = $name_subject;
                $this->db->insert($this->_table_subject_directory,$data_subject);
                $result['subject_id'] = $this->db->insert_id();
            }
            elseif (key_exists($name_subject,$change_subject)){
                $result['subject_id'] = $change_subject[$name_subject];
            }
            else{
                $result['subject_id'] = 1620;
            }
        }
        else $result['subject_id'] = $subj['id'];

        return $result['subject_id'];
    }

    /**
     * Импорт ссылок
     *
     * @param string $path
     * @return int
     */
    public function importCsvAhrefsLinks ($path)
    {
        $data_csv = [];
        $row = 0; // строка
        $count_before_insert = $this->db
            ->select('id')
            ->get($this->_table_ahrefs_links)
            ->num_rows();
        if (($handle = fopen($path, 'r')) !== FALSE) {
            while (($data = fgetcsv($handle, 0, "\t")) !== FALSE) {
                if (!isset($data[1])) continue;
                if ($row == 0) {
                    $row++;
                    continue;
                }
                $data_csv[] = array(
                    'flag_anchor' => ($data[7] == $data[9] || empty($data[9])) ? 0 : 1,
                    'url_rating' => $data[1],
                    'domain_rating' => $data[2],
                    'reffering_page_url' => $data[3],
                    'reffering_page_title' => $data[4],
                    'internal_links_count' => $data[5],
                    'external_links_count' => $data[6],
                    'link_url' => $data[7],
                    'text_pre' => $data[8],
                    'link_anchor' => $data[9],
                    'text_post' => $data[10],
                    'type' => $data[11],
                    'back_link_status' => $data[12],
                    'first_seen' => !empty($data[13]) ? date(FORMAT_DATETIME, strtotime($data[13])) : null,
                    'last_check' => !empty($data[14]) ? date(FORMAT_DATETIME, strtotime($data[14])) : null,
                    'day_lost' => !empty($data[15]) ? date(FORMAT_DATETIME, strtotime($data[15])) : null,
                    'language' => $data[16],
                );
            }
            fclose($handle);
        }

        if ($data_csv) {
            foreach (array_chunk($data_csv, 500) as $to_insert_part) {
               $datas[]=  $this->insertIgnoreBatch($this->_table_ahrefs_links, $to_insert_part);
            }
        }
        $count_after_insert = $this->db
            ->select('id')
            ->get($this->_table_ahrefs_links)
            ->num_rows();
        unlink($path);
        return ($count_after_insert - $count_before_insert);
    }

}