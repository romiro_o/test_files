<?php
/**
 * @see Linkbuilding::work
 */
?>

<div class="col-md-9" >
	<div class="block">
		<div class="block-title">
			<h2>Фильтры:</h2>
		</div>
		<form class="form-horizontal form-bordered" method="get" >

			<div class="form-group row">
				<div class="col-md-6">
					<?= form_multiselect('ids[]', $array_types_links, $get_types_links, [
						'class' => 'select-chosen',
						'data-placeholder' => 'Выбот типы ссылок',
					])?>
				</div>
				<div class="col-md-2">
					<input name="limit" class="form-control" min="10" max="100" type="number" value="<?= $limit;?>"/>
				</div>
				<div class="col-md-4 ">
					<button class="btn btn-block btn-primary" type="submit">Выбрать</button>
				</div>

			</div>
		</form>
	</div>
	<div class="block">
		<div class="block-title">
			<h2>Рекомендуемые сайты для размещения ссылок:</h2>
		</div>
		<?php if($datas): ?>
		<div class="table-responsive">

			<table id="project-datatable" class="table table-vcenter table-condensed table-bordered">
				<thead>
				<tr>
					<th>Донор</th>
					<th>Только <br>безанкорная <br>ссылка</th>
					<th>Тип ссылки</th>
					<th>Логин пароль,<br>Форма входа</th>
					<th>Цена</th>
					<th></th>
				</tr>
				</thead>
				<tbody>
                <?php foreach($datas as $data):?>
				<?php if((isset($data['ready_donors_wo_anchor']) && $data['ready_donors_wo_anchor'])){
						$j = true; ?>
					<tr>
						<td colspan="6" class="success text-center">Безанкорные</td>
					</tr>
					<?php for($i=0; $i<count($data['ready_donors_wo_anchor']); $i++) { ?>
					<tr>
						<td>
							<a href="<?= $data['ready_donors_wo_anchor'][$i]['url'] ?>" target="_blank">
								<?= $data['ready_donors_wo_anchor'][$i]['url']; ?>
							</a>
						</td>
						<td><?= $data['ready_donors_wo_anchor'][$i]['only_wo_anchor_link'] ? 'Да' : 'Нет'; ?></td>
						<td><?= $data['ready_donors_wo_anchor'][$i]['name']; ?></td>
						<td>
							<?= isset($data['ready_donors_wo_anchor'][$i]['login'])? $data['ready_donors_wo_anchor'][$i]['login']:''; ?><br>
							<?= isset($data['ready_donors_wo_anchor'][$i]['password'])? $data['ready_donors_wo_anchor'][$i]['password']:''; ?><br>
							<?= isset($data['ready_donors_wo_anchor'][$i]['login_url'])? $data['ready_donors_wo_anchor'][$i]['login_url']:''; ?><br>
						</td>
						<td><?= $data['ready_donors_wo_anchor'][$i]['price']; ?></td>
						<td>
							<a class="btn btn-primary  btn-block" target="_blank"
							   href="/linkbuilding/links/<?=
							   $project_id?>/add/null/<?=
							   $data['ready_donors_wo_anchor'][$i]['id']?>/<?=
							   $data['ready_donors_wo_anchor'][$i]['type_links_id']?>/<?=
                               $operation_check ? $operation_check['id']: ''?>">Разместить
							</a>
						</td>
					</tr>
					<?php  } ?>
					<?php  } ?>

				<?php if(isset($data['ready_donors_anchor']) && $data['ready_donors_anchor']){
					$j = true;
					?>
					<tr>
						<td colspan="6" class="success text-center">Анкорные</td>
					</tr>
						<?php for($i=0; $i<count($data['ready_donors_anchor']); $i++) { ?>
							<tr>
								<td>
									<a href="<?= $data['ready_donors_anchor'][$i]['url'] ?>" target="_blank">
										<?= $data['ready_donors_anchor'][$i]['url']; ?>
									</a>
								</td>
								<td><?= $data['ready_donors_anchor'][$i]['only_wo_anchor_link'] ? 'Да' : 'Нет'; ?></td>
								<td><?= $data['ready_donors_anchor'][$i]['name']; ?></td>
								<td>
									<?= isset($data['ready_donors_anchor'][$i]['login'])? $data['ready_donors_anchor'][$i]['login']:''; ?><br>
									<?= isset($data['ready_donors_anchor'][$i]['password'])? $data['ready_donors_anchor'][$i]['password']:''; ?><br>
									<?= isset($data['ready_donors_anchor'][$i]['login_url'])? $data['ready_donors_anchor'][$i]['login_url']:''; ?><br>
								</td>
								<td><?= $data['ready_donors_anchor'][$i]['price']; ?></td>
								<td>
									<a class="btn btn-primary btn-block" target="_blank"
									   href="/linkbuilding/links/<?=
									   $project_id?>/add/null/<?=
									   $data['ready_donors_anchor'][$i]['id']?>/<?=
									   $data['ready_donors_anchor'][$i]['type_links_id']?>/<?=
                                       $operation_check ? $operation_check['id']: ''?>">
                                        Разместить
									</a>
								</td>
							</tr>
						<?php  } ?>
					<?php  } ?>
                <?php  endforeach; ?>
				
				<?php if(!$j) : ?>
					<tr>
						<td colspan="5" class="danger text-center">
							Нет рекомендованных ссылок для выбранных значений.
						</td>
					</tr>
				<?php endif; ?>
				</tbody>
			</table>
		</div>
		<?php else: ?>
			<div class="block">
				<div class="text-center">
					Нет рекомендуемых сайтов для размещения ссылок.
				</div>

			</div>
		<?php endif; ?>
	</div>
</div>

<div class="col-md-3">
    <!-- Pie Chart Block -->
    <?php if($count_urls): ?>
    <div class="block">
        <!-- Pie Chart Title -->
        <div class="block-title">
            <h2><strong>Типы ссылок в процентах</strong></h2>
        </div>
        <!-- END Pie Title -->

        <!-- Pie Chart Content -->
        <div id="chart-pie" class="chart"></div>
        <!-- END Pie Chart Content -->
    </div>
    <!-- END Pie Chart Block -->
    <div class="block">

        <div class="block-title">
            <h2><strong>Статистика</strong></h2>
        </div>
        <div class="table-responsive">

            <table id="project-datatable" class="table table-vcenter table-condensed table-bordered">
                <thead>
                <tr>
                    <td>Тип ссылки</td>
                    <td class="text-right"><span title="Анкорные">А</span></td>
                    <td class="text-right"><span title="Безанкорные">B</span></td>
                    <td class="text-right"><span title="Анкорные и Безанкорные">АБ</span></td>
                </tr>
                </thead>
                <tbody>
                    <?php foreach($count_urls['data'] as $value): ?>
                        <tr>
                            <td><?= $value['name'];?></td>

                            <td class="text-right"><?= $value['all_urls']-$value['urls_wo_anchor'];?></td>
                            <td class="text-right"><?= $value['urls_wo_anchor'];?></td>
                            <td class="text-right"><?= $value['all_urls'];?></td>
                        </tr>
                    <?php endforeach; ?>
                    <tr>
                        <td class="text-right">Итого:</td>
                        <td class="text-right"><?= $count_urls['all_urls']-$count_urls['all_wo_anchor'];?></td>
                        <td class="text-right"><?= $count_urls['all_wo_anchor'];?></td>
                        <td class="text-right"><?= $count_urls['all_urls'];?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <?php else: ?>
        <div class="block">
            <div class="block-title">
                <h2><strong>Типы ссылок в процентах</strong></h2>
            </div>
            <div class="text-center">
                Нет ссылок у проекта.
            </div>

        </div>
    <?php endif; ?>
</div>


<script>$(function(){ CompCharts.init(); });</script>
<script>
    /*
     *  Document   : compCharts.js
     *  Author     : pixelcave
     *  Description: Custom javascript code used in Charts page
     */

    var CompCharts = function() {

        return {
            init: function() {

                var chartPie = $('#chart-pie');

                // Pie Chart
                $.plot(chartPie,
                    [
                        <?php foreach ($count_urls['data'] as $value):?>
                        {
                            label: '<?= $value['name'];?>',
                            data: <?= round((100*$value['all_urls'])/$count_urls['all_urls'])?>
                        },
                        <?php endforeach; ?>
                    ],
                    {
                        colors: [
                            <?php
                                $colors = array (
                                    '#00ff00','#cc0099','#ff0099','#00cccc','#0099ff','#00ffff','#9900cc','#9966ff',
                                    '#990066','#660000','#cc0000','#ff6666','#ff3300','#ff6600','#ffcc00','#ffff00','#006600',
                                    '#3366ff','#330066','#660099','#66ffcc','#003399','#0033ff',
                                    '#ff00ff','#ff66ff','#006666'
                                );
                                for($i=0; $i< count($count_urls['data']); $i++){?>
                            '<?= $colors[$i] ?>',
                            <?php } ?>

                        ],
                        legend: {show: false},
                        series: {
                            pie: {
                                show: true,
                                radius: 1,
                                label: {
                                    show: true,
                                    radius: 5/6,
                                    formatter: function(label, pieSeries) {
                                        return '<div class="chart-pie-label" ">' +
                                            '<a  style="cursor: pointer; color: black; text-decoration: none" title="'+
                                            label+'">'+ Math.round(pieSeries.percent) + '%</a></div>';
                                    },
                                    background: {opacity: 0, color: '#000000'}
                                }
                            }
                        }
                    }
                );


            }
        };
    }();

</script>