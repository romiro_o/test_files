<?php
/**
 * Class Directory_methods_model
 *
 * @author orn@fivefaces.ru.
 */

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Навигация по методам - модель для построения дерева
 */
class Directory_methods_model extends main_model {

    /**
     * Таблица хранения словаря методов
     * @type string
     */
    public $table_directory_methods;

    /**
     * Таблица хранения словаря методов проекта
     * @type string
     */
    public $table_directory_project_methods;

    /**
     * Таблица связи методов с ролями
     * @type string
     */
    public $table_directory_methods_roles;

    /**
     * Таблица связи методов проектов с ролями
     * @type string
     */
    public $table_directory_project_methods_roles;

    /**
     * Таблица связи методов проектов с сервисами
     * @type string
     */
    public $table_directory_project_methods_services;

    /**
     * @var
     */
    public $role_id = 9;

    /**
     * @var
     */
    public $user_id;

    /**
     * @var
     */
    public $roles_allowed_uris = [];

    /**
     * Активность пункта меню
     * @var
     */
    public $current = 0;

    /**
     * Directory_methods_model constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->table_directory_methods = $this->getTableName('directory_methods');
        $this->table_directory_methods_roles = $this->getTableName('directory_methods_roles');
        $this->table_directory_project_methods = $this->getTableName('directory_project_methods');
        $this->table_directory_project_methods_roles = $this->getTableName('directory_project_methods_roles');
        $this->table_directory_project_methods_services = $this->getTableName('directory_project_methods_services');
    }

    /**
     * Устанавливаем пользователя и его роль
     * @param null $user_id
     * @return $this
     */
    public function setUserId($user_id = null)
    {
        if(!$user_id){
            if(!$this->dx_auth->is_logged_in()){
                $this->load->model('dx_auth/roles');
                $this->role_id = $this->roles->get_role_by_name(ROLE_GUEST)->row_array()['id'];
                return $this;
            }
            $this->user_id = $this->dx_auth->get_user_id();
        }

        $user = $this->users->get_user_by_id($this->user_id);
        $this->role_id = $user->row()->role_id;

        $this->load->model('dx_auth/permissions', 'permissions');
        $roles_allowed_uris = $this->permissions->get_permission_value(
            $this->role_id, 'uri'
        );
        $this->roles_allowed_uris = array_map('trim', $roles_allowed_uris);

        return $this;
    }

    /**
     * Вернет дерево методов доступное пользователю
     * @param $search
     * @return array
     */
    public function methodsTree($search = '')
    {
        //Получаем доступные методы
        $methods = $this->getActiveMethod($search);
        $methods = array_map(function ($row) {
            return $row + ['current' => 0, 'visible' => 0];
        }, $methods);

        $methods_tree = [];

        $prepare_methods = arrayIndexed($methods,'id');
        $prepare_methods =  $this->checkPermission($prepare_methods);

        if($prepare_methods){
            //Получаем дерево методов
            $methods_tree = $this->getTree($prepare_methods);
        }

        return $methods_tree;
    }

    /**
     * Вернет активные и доступные пользователю методы
     * @param $search
     * @return mixed
     */
    public function getActiveMethod($search = '')
    {
        if($this->role_id){
            $this->setUserId();
        }

        if($search) {
            $this->db->like("dm.title", $search);
            $this->db->or_like("dm.descriptions", $search);
            $this->db->or_like("dm.url", $search);
        }

        $result = $this->db
            ->select("dm.*")
            ->where("dm.active", true)
            ->join("directory_methods_roles as dmr", "dmr.directory_methods_id = dm.id 
                and dmr.role_id = $this->role_id")
            ->order_by('dm.sort', 'ASC')
            ->get("$this->table_directory_methods as dm")
            ->result_array();
        return $result;
    }

    /**
     * Формирование дерева методов
     * @param $data
     * @param $project_id
     * @return array
     */
    public function getTree($data, $project_id = '') {
        $tree = [];
        $ids = array_column($data, 'id');
        foreach ($data as $id => &$node) {
            //проверка на активность пункта меню
            if($this->checkCurrentPage($node['menu_key'].$project_id, $node['url'].$project_id)){
                $this->setCurrent($data, $node['id']);
                ++$this->current;
            }
            //Если нет родителя
            if (!arrayGet('parent_id', $node, 0)){
                if(empty($node['url'])) {
                    $node['url'] = $project_id ? '' : $node['url'];
                } else {
                    $node['url'] = $node['url'].$project_id;
                }
                $node['menu_key'] = $node['menu_key'].$project_id;
                $tree[$id] = &$node;
            }else{
                //если родитель активен и доступен пользователю
                if(in_array($node['parent_id'], $ids)){
                    //Если есть потомки то перебераем массив
                    $node['url'] = $node['url'].$project_id;
                    $node['menu_key'] = $node['menu_key'].$project_id;
                    $data[$node['parent_id']]['children'][$id] = &$node;
                }

            }
            unset($node);
        }
        return $tree;
    }

    /**
     * Выборка одного метода
     * @param integer $method_id
     * @param integer $project_id
     * @return mixed
     */
    public function getMethodOne($method_id, $project_id = null)
    {
        if($this->role_id){
            $this->setUserId();
        }

        $table = !$project_id ? $this->table_directory_methods : $this->table_directory_project_methods;
        $table_role = !$project_id ? $this->table_directory_methods_roles : $this->table_directory_project_methods_roles;
        $key = !$project_id ? 'directory_methods_id' : 'directory_project_methods_id';

        return $this->db
        ->select("dm.*")
        ->where("dm.id", $method_id)
        ->where("active", true)
        ->join("$table_role as dmr", "dmr.$key = dm.id 
                and dmr.role_id = $this->role_id")
        ->get("$table as dm")
        ->row_array();
    }

    /**
     * Дерево одного родителя
     * @param $parent_id
     * @param $project_id
     * @param string $search
     * @return array
     */
    public function getTreeOneParent($parent_id, $search = '', $project_id = null)
    {
        $result = $tree_methods = [];
        if($this->role_id){
            $this->setUserId();
        }
        if($project_id) {
            $this->load->model('constructor_reports/constructor_reports_pages_model');
            $project = $this->project_model->getOne($project_id);
            $methods = $this->getActiveProjectMethods();
            $project_methods = $this->getTree($methods, $project_id);
            $constructor_reports_menu = $this->constructor_reports_pages_model->getMenus(getCurrentRoleId());
            if (!$parent_id) {
                return $this->prepareProjectMethodsTree($project_id, $project['service'], $project_methods, $constructor_reports_menu);
            } else {
                $project_methods = array_intersect_key($project_methods, [$parent_id=>'']);
                $methods_tree = $this->prepareProjectMethodsTree($project_id, $project['service'], $project_methods, $constructor_reports_menu);
                return $methods_tree ? arrayGet('children', $methods_tree[0], []) : [];
            }
        }
        else{
            $methods = $this->getActiveMethod($search);
            $methods = arrayIndexed($methods,'id');
            $methods_tree = $this->getChildrenTree($methods, $parent_id, $project_id);
        }
        if($methods_tree){
            foreach($methods_tree as $row){
                if($row['parent_id'] == $parent_id){
                    $row['parent_id'] = null;
                }
                $tree_methods[] = $row;
            }
            $methods_tree = arrayIndexed($tree_methods,'id');
            $result = $this->getTree($methods_tree, $project_id);
        }

        return $result;
    }

    /**
     * Вернет дерево потомков родителя
     * @param $data
     * @param $parent_id
     * @param $project_id
     * @return array|bool
     */
    public function getChildrenTree($data, $parent_id, $project_id = null)
    {
        if (arrayGet('parent_id', $data, 0)){
            return false;
        }
        $tree = [];

        foreach ($data as $row) {
            if ($row['parent_id'] == $parent_id) {
                $tree[] = $row;
                $tree = array_merge($tree,$this->getChildrenTree($data, $row['id'], $project_id));
            }
        }

        return $tree;
    }

    /**
     * Вернет методы нулевого уровня
     * @return mixed
     */
    public function getParentMethods()
    {
        return $this->db
            ->select("dm.*")
            ->where("dm.parent_id", 0)
            ->or_where("dm.parent_id", null)
            ->where("active", true)
            ->join("directory_methods_roles as dmr", "dmr.directory_methods_id = dm.id 
                and dmr.role_id = $this->role_id")
            ->order_by('sort', 'ASC')
            ->get("$this->table_directory_methods as dm")
            ->result_array();
    }

    /**
     * Вернет активные и доступные пользователю методы проектов
     * @param string $search
     * @return mixed
     */
    public function getActiveProjectMethods(string $search = '')
    {
        if($this->role_id){
            $this->setUserId();
        }
        if($search) {
            $this->db->like("dmp.title", $search);
            $this->db->or_like("dmp.descriptions", $search);
            $this->db->or_like("dmp.url", $search);
        }
        $prepare_methods = [];
        $result = $this->db
            ->select("dmp.*")
            ->where("active", true)
            ->join("directory_project_methods_roles as dmpr", "dmpr.directory_project_methods_id = dmp.id 
                and dmpr.role_id = $this->role_id")
            ->order_by('sort', 'ASC')
            ->get("$this->table_directory_project_methods as dmp")
            ->result_array();

        if($result){
            $methods = array_map(function ($row) {
                return $row + ['current' => 0, 'visible' => 0];
            }, $result);
            $prepare_methods = arrayIndexed($methods,'id');
            $prepare_methods =  $this->checkPermission($prepare_methods);
        }

        return $prepare_methods;
    }

    /**
     * Вернет массив услуг всех методов проектов
     * @return mixed
     */
    public function getServicesProjectMethods()
    {
        $services = [];
        $result = $this->db
            ->get($this->table_directory_project_methods_services)
            ->result_array();
        foreach($result as $row){
            $services[$row['directory_project_methods_id']][] = $row['service_id'];
        }
        return $services;
    }

    /**
     * Вернет объеденененное дерево методов проекта
     * @param $project_id
     * @param $project_methods
     * @param $project_service
     * @param $constructor_reports_menu
     * @return mixed
     */
    public function prepareProjectMethodsTree($project_id, $project_service, array $project_methods = [], array $constructor_reports_menu = [])
    {
        $services = $this->getServicesProjectMethods();
        foreach($project_methods as $key => &$method){
            $current_project = 0;
            $menu = [];
            //пропускаем если не соответвует услуге или нет услуги - Все
            if(!in_array($project_service, $services[$method['id']]) && !in_array(SERVICE_ALL, $services[$method['id']])){
                unset($project_methods[$key]);
                continue;
            }
            if($method['has_constructor_pages'] ){
                if(isset($constructor_reports_menu[$method['id']])) {
                    foreach ($constructor_reports_menu[$method['id']] as $constructor) {

                        if($constructor['project_id'] && $constructor['project_id'] != $project_id){
                            continue;
                        }
                        if($constructor['project_id']){
                            $slug_array =  explode('_', $constructor['slug']);
                            $link = sprintf('project/user_report/%s/%s', $project_id, $slug_array[1]);
                        }
                        else{
                            $link = sprintf('project/report/%s/%s', $constructor['slug'], $project_id);
                        }
                        $current = ($this->checkCurrentPage('project_report_' . $constructor['slug'] .  $project_id, $link )) ? 1 : 0;
                        $current_project = $current ? ++$current_project : $current_project;
                        $menu[] = [
                            'title' => $constructor['menu_title'],
                            'url'   => $link,
                            'menu_key'   => 'project_report_' . $constructor['slug'] .  $project_id,
                            'current' => $current,
                            'sort'  => $constructor['menu_sort'],
                            'id'    => $method['id'],
                            'project_id' => $project_id,
                            'is_index_page' => 0,
                            'descriptions' => $constructor['menu_title'],
                        ];
                        $this->current = $current_project ? ++$this->current : $this->current;
                    }

                }
            }
            if(isset($method['children'])){
                //удаляем если не соответвует услуге
                foreach($method['children'] as $key_child => $row){
                    if(!in_array($project_service, $services[$row['id']]) ){
                        unset($project_methods[$method['id']]['children'][$key_child]);
                    }
                }
                $method['children'] = $method['children'] + $menu;
            }
            elseif($menu){
                $project_methods[$method['id']]['children'] = $menu;
            }

            $method['current'] = !$method['current'] ? $current_project : $method['current'];
            $method['project_id'] = $project_id;
            unset($method);
        }
        if($project_methods) {
            collectionSort($project_methods, [
                'sort' => SORT_ASC
            ]);
        }
        return $project_methods;
    }

    /**
     * Формирует дерево меню проектов плюс меню страниц из конструктора
     * @return array
     */
    public function prepareProjectMenu()
    {
        $this->load->model('constructor_reports/constructor_reports_pages_model');
        $projects = $this->project_model->projectsEnableView();
        $constructor_reports_menu = $this->constructor_reports_pages_model->getMenus(getCurrentRoleId());
        $menu = [];
        //Получаем доступные методы
        $methods = $this->getActiveProjectMethods();

        if($methods){
            $methods = array_map(function ($row) {
                return $row + ['project_id' => 0];
            }, $methods);
            foreach($projects as $project){
                $this->current = 0;
                $project_methods = $this->getTree($methods, $project['id']);
                $menu[$project['id']] = [
                    'title'=> $project['name_alt'],
                    'menu_key' => 'project_' . $project['id'],
                    'url' => '#',
                    'children' => $this->prepareProjectMethodsTree($project['id'], $project['service'], $project_methods, $constructor_reports_menu),
                    'current' => $this->current ? 1 : ($this->checkCurrentPage('project_report'.$project['id'], 'project/view/'.$project['id'])) ? 1 : 0,
                    'is_index_page' => 1,
                    'project_id' => $project['id'],
                    'descriptions' => $project['name_alt'],
                ];
            }
        }
        if($this->checkPermissionUrl('/project/')){
            $current = ($this->checkCurrentPage('projects_my', 'project')) ? 1 : 0;
            $this->current = $current ? ++$this->current : $this->current;
            $menu[] = [
                'title'=> 'Все проекты',
                'menu_key' => 'projects_my',
                'url' => 'project',
                'children' => [],
                'current' => $current,
                'is_index_page' => 0,
                'descriptions' => 'Все проекты',
            ];
        }

        if($this->checkPermissionUrl('project/archive')){
            $current = ($this->checkCurrentPage('projects_archive', 'project/archive')) ? 1 : 0;
            $this->current = $current ? ++$this->current : $this->current;
            $menu[] = [
                'title'=> 'Архив проектов',
                'menu_key' => 'projects_archive',
                'url' => 'project/archive',
                'children' => [],
                'current' => $current,
                'is_index_page' => 0,
                'descriptions' => 'Архивные проекты',
            ];
        }

        return $menu;
    }

    /**
     * Вернет полное дерево меню
     * @return array
     */
    public function prepareMainMenu()
    {
        //дерево методов
        $methods_tree = $this->methodsTree();
        $main_menu = [
            [
                'title'    => 'Dashboard',
                'menu_key' => 'dashboard',
                'url'      => 'dashboard',
                'current'  => ($this->checkCurrentPage('dashboard',
                    'dashboard')) ? 1 : 0,
                'visible'  => 1,
            ]
        ];

        foreach($methods_tree as $parent){
            $has_menu = true;
            switch(strtolower($parent['menu_key'])){
                case 'header_projects':
                    $children = $this->prepareProjectMenu();
                    $has_menu = $children ? true : false;
                    $parent['children'] = $children;
                    //var_dump($parent['children'] );exit;

                    break;
                case 'tools':
                    $this->current = 0;
                    $tools = $this->prepareToolsMenu();
                    $methods_tree[$parent['id']]['current'] = $this->current;
                    $parent['children'] = arrayGet('children', $methods_tree[$parent['id']], []) + $tools;
                    break;
                default:
                    if($children = arrayGet('children', $methods_tree[$parent['id']], [])){
                        $parent['children'] = $children;
                    }
                    break;
            }
            if($has_menu){
                $main_menu[] = $parent;
            }
        }

        return $main_menu;
    }

    /**
     * Формирует дерево меню инструментов
     * @return array
     */
    public function prepareToolsMenu()
    {
        //инструменты
        $this->load->model('backend/backend_model');
        $public_tools = $this->backend_model->getPublicTools();
        $public_tools = \Underscore\Types\Arrays::group($public_tools, 'group_name');
        $tools = [];
        foreach ($public_tools as $k_data => $v_data) {
            $tool = [];
            $current_tools = 0;
            foreach($v_data as $item)
            {
                //устанавливаем активность пункта меню по адресу страницы
                $current = ($this->checkCurrentPage($item['name'], 'tools/'. $item['url'])) ? 1 : 0;
                $current_tools = $current ? ++$current_tools : $current_tools;

                $tool[] =
                    [
                        'title' =>$item['name'],
                        'url' =>'tools/'. $item['url'],
                        'menu_key' => $item['name'],
                        'current' => $current,
                        'visible'=>$item['public'],
                        'sort' => 100000
                    ];
            }
            $tools[] = [
                'title'=> $k_data,
                'menu_key' => $v_data[0]['alias'],
                'current' => $current_tools,
                'url' => '#',
                'visible'=> 1,
                'children' => $tool,
                'sort' => 100000
            ];
            $this->current = $current_tools ? ++$this->current : $this->current;
        }
        return $tools;
    }

    /**
     * Проверяет соответвие текущей странице
     * @param $key
     * @param $url
     * @return int
     */
    public function checkCurrentPage($key, $url)
    {
        $current = ( str_replace('/', '', $this->uri->uri_string()) == str_replace('/', '', $url)
            || strtolower($key) === strtolower($this->config->item('menu_key')) ) ? 1 : 0;
        return $current;
    }

    /**
     * Установит потомку и родителям потомка current - активность для меню
     * Вернет цепочку от потомка к главному родителю
     * Установит видимость пункта меню от потомка к главному родителю
     * @param $methods
     * @param $id
     * @param $project_id
     * @param $visible
     * @param array $response
     * @return array|null
     */
    public function setCurrent(&$methods, $id, $response = [], $project_id = '', $visible = false)
    {
        $key = null;
        foreach ($methods as $index => $val) {
            if ($val['id'] === $id) {
               $key = $index;
               break;
            }
        }
        if($key){
            if($project_id){
                $methods[$key]['url'] = $methods[$key]['url'].$project_id;
            }
            $response[] = $methods[$key];
            $methods[$key]['current'] = $visible ? 0 : 1;
            $methods[$key]['visible'] =  1;
            if ($methods[$key]['parent_id'] != 0)
            {
                $response = $this->setCurrent($methods, $methods[$key]['parent_id'], $response, $project_id, $visible);
            }
            return $response;
        }
        return null;
    }

    /**
     * Вернет активные и доступные пользователю методы
     * @param $project_id
     * @return mixed
     */
    public function searchMethodByUrl($project_id = '')
    {
        $search_url = $this->uri->uri_string();

        //поиск без аргумента метода(id проекта)
        $url_info = explode('/',$search_url);
        $menu_key = strtolower($this->config->item('menu_key'));
        //ключ меню без id проекта
        $key = substr($menu_key, 0, strcspn($menu_key, '0123456789'));
        $data = [];

        $this->db->where("url", $search_url);

        if($menu_key){
            $this->db->or_where("menu_key", $menu_key);
        }
        else{
            $this->db->or_like("url", $search_url, 'before');
        }

        //поиск в словаре общих методов приоритет по url
        $result = $this->db
            ->order_by("url like '%$search_url%' DESC")
            ->get($this->table_directory_methods)
            ->row_array();
        //если нет в словаре общих методов ищем в словаре методов проекта
        if(!$result){

            $this->db->like("url", $search_url, 'before');
            $url = '';
            if($url_info){
                //формируем url для поиска без аргумента id проекта
                foreach($url_info as $part_url){
                    if(!is_numeric($part_url)){
                        $url .= $part_url . '/';
                    }
                    else{
                        $project_id = $project_id ? $project_id : $part_url;
                        $this->db->or_like("url", $url);
                        break;
                    }
                }
            }
            $this->db->or_where("menu_key", $key);
            //поиск в словаре методов проектов приоритет по url
            $result = $this->db
                ->order_by("url like '%$search_url%' DESC")
                ->order_by("url like '%$url%' DESC")
                ->get($this->table_directory_project_methods)
                ->row_array();
            $methods = $this->db
                ->get($this->table_directory_project_methods)
                ->result_array();
            if(!$result && $project_id){
                $this->load->model('constructor_reports/constructor_reports_pages_model');
                $methods = $this->getActiveProjectMethods();
                $constructor_reports_menu = $this->constructor_reports_pages_model->getMenus(getCurrentRoleId());
                $parent_id = 0;
                foreach($constructor_reports_menu as $_key => $constructor){
                    foreach ($constructor as $row){
                        if($row['project_id']){
                            $slug_array =  explode('_', $row['slug']);
                            $link = sprintf('project/user_report/%s/%s', $project_id, $slug_array[1]);
                        }
                        else{
                            $link = sprintf('project/report/%s/%s', $row['slug'], $project_id);
                        }
                        if( $search_url == $link){
                            $data[] = [
                                'page_title' => null,
                                'title' => $row['menu_title'],
                                'url'   => $link,
                                'menu_key'   => 'project_report_' . $row['slug'] .  $project_id,
                                'sort'  => $row['menu_sort'],
                                'project_id' => $project_id,
                                'is_index_page' => 0,
                                'parent_id' => $_key,
                                'descriptions' => $row['description'],
                            ];
                            $parent_id = $_key;
                            break;
                        }
                    }
                }
                if($parent_id){
                    $data[] = $methods[$parent_id];
                }
                return $data;
            }
        }
        else{
            //выбор всех общих методов
            $methods = $this->db
                ->get($this->table_directory_methods)
                ->result_array();
        }

        //получаем цепочку потомок - родители
        if($result){
            $prepare_methods = arrayIndexed($methods,'id');
            $data = $this->setCurrent($prepare_methods, $result['id'], [], $project_id);
        }
        return $data;
    }

    /**
     * Проверит разрешен ли пункт меню к показу для текущего пользователя
     * @param $uri
     * @return bool
     */
    public function checkPermissionUrl($uri)
    {
        if(!in_array('/', $this->roles_allowed_uris)){
            $uri_part = explode('/', $uri);
            $uri_part = array_map('trim', $uri_part);
            $url = '/';
            foreach($uri_part as $row){
                $url .= $row.'/';
                if(in_array($url, $this->roles_allowed_uris)){
                    return true;
                }
            }
        }
        else{
            return true;
        }
        return false;
    }

    /**
     * Вернет массив методов разрешенной роли текущего  пользователя
     * @param $data
     * @return array
     */
    public function checkPermission($data)
    {
        foreach($data as $row){
            if($row['url']=='header') $data[$row['id']]['visible'] = 1;
            if($this->checkPermissionUrl($row['url'])){
                $this->setCurrent($data, $row['id'], [], '', true);
            }
        }
        return \Underscore\Types\Arrays::filter($data, function ($row) {
            return ($row['visible']);
        });
    }

}